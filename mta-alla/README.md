10.3141/2540-11
10.3141/2535-01
10.3141/2538-03
10.3141/2415-03

10.3141/2353-04

10.3141/2353-06

10.3141/2274-04

10.3141/2216-03

10.3141/2216-10

10.3141/2143-07

10.3141/2144-10

10.3141/2163-05

10.3141/2110-16

10.3141/2111-15
10.3141/2112-12


Time-Expanded Network Model of Train-Level Subway Ridership Flows Using Actual Train Movement Data
Timon Stasko, Brian Levine, and Alla Reddy
Transportation Research Record: Journal of the Transportation Research Board 2016 2540:, 92-101 

Development of Application for Estimating Daily Boarding and Alighting Counts on New York City Buses
Qifeng Zeng, Alla Reddy, Alex Lu, and Brian Levine
Transportation Research Record: Journal of the Transportation Research Board 2015 2535:, 1-14 

Development of a Real-Time Stringlines Tool to Visualize Subway Operations and Manage Service at New York City Transit
Boris Suchkov, Mikhail Boguslavsky, and Alla Reddy
Transportation Research Record: Journal of the Transportation Research Board 2015 2538:, 19-29 

Designing New York City Subways' Key Performance Indicators to Improve Service Delivery and Operations
Alla Reddy, Alex Lu, Mikhail Boguslavsky, and Brian Levine
Transportation Research Record: Journal of the Transportation Research Board 2014 2415:, 21-34 

Observed Customer Seating and Standing Behavior and Seat Preferences on Board Subway Cars in New York City
Aaron Berkovich, Alex Lu, Brian Levine, and Alla Reddy
Transportation Research Record: Journal of the Transportation Research Board 2013 2353:, 33-46 

Measurement of Subway Service Performance at New York City Transit
Brian Levine, Alex Lu, and Alla Reddy
Transportation Research Record: Journal of the Transportation Research Board 2013 2353:, 57-68 

Strategic Look at Friday Exceptions in Weekday Schedules for Urban Transit
Alex Lu and Alla Reddy
Transportation Research Record: Journal of the Transportation Research Board 2012 2274:, 30-51 

Algorithm to Measure Daily Bus Passenger Miles Using Electronic Farebox Data for National Transit Database Section 15 Reporting
Alex Lu and Alla Reddy
Transportation Research Record: Journal of the Transportation Research Board 2011 2216:, 19-32 

Measuring and Controlling Subway Fare Evasion
Alla Reddy, Jacqueline Kuhls, and Alex Lu
Transportation Research Record: Journal of the Transportation Research Board 2011 2216:, 85-99 

Subway Productivity, Profitability, and Performance
Alla Reddy, Alex Lu, and Ted Wang
Transportation Research Record: Journal of the Transportation Research Board 2010 2143:, 48-58 

Using Quantitative Methods in Equity and Demographic Analysis to Inform Transit Fare Restructuring Decisions
Robert Hickey, Alex Lu, and Alla Reddy
Transportation Research Record: Journal of the Transportation Research Board 2010 2144:, 80-92 

Safeguarding Minority Civil Rights and Environmental Justice in Service Delivery and Reductions
Alla Reddy, Thomas Chennadu, and Alex Lu
Transportation Research Record: Journal of the Transportation Research Board 2010 2163:, 45-56 

Entry-Only Automated Fare-Collection System Data Used to Infer Ridership, Rider Destinations, Unlinked Trips, and Passenger Miles
Alla Reddy, Alex Lu, Santosh Kumar, Victor Bashmakov, and Svetlana Rudenko
Transportation Research Record: Journal of the Transportation Research Board 2009 2110:, 128-136 

Performance Measurements on Mass Transit
Anthony Cramer, John Cucarese, Minh Tran, Alex Lu, and Alla Reddy
Transportation Research Record: Journal of the Transportation Research Board 2009 2111:, 125-138 

Passenger Environment Survey
Alex Lu, Steven Aievoli, Joseph Ackroyd, Chrissie Carlin, and Alla Reddy
Transportation Research Record: Journal of the Transportation Research Board 2009 2112:, 93-103 
